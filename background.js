// Called when the url of a tab changes.
function checkForValidUrl(tabId, changeInfo, tab) {
  if (tab.url.match(/.*.shutterfly.com\/(\d)+/) || tab.url.match(/.*.shutterfly.com\/pictures\/(\d)+/) ) {
    if (changeInfo.status === 'complete') {
      chrome.tabs.executeScript(tabId, {'file':'lib/albumDetailSnippet.js'});
      chrome.pageAction.show(tabId);
    }
  }
};

// Listen for any changes to the URL of any tab.
chrome.tabs.onUpdated.addListener(checkForValidUrl);
