function ShutterflyBackupService(collectionKey, albumKey, site) {
  this.collectionKey = collectionKey;
  this.albumKey = albumKey;
  this.site = site;
  this.totalDownloaded = 0;
}

ShutterflyBackupService.prototype.fixJSON = function(brokenJSON){
  console.log("[fixJSON]");
  console.log(brokenJSON);
  var durable = durableJsonLint(brokenJSON);
  return durable.json;
}

ShutterflyBackupService.prototype.downloadCurrentPage = function(data, status) {
  console.log("[downloadCurrentPage]");
  var res = data.responseText;
  var hash;

  res = this.fixJSON(res);
  console.log("Parsing result");
  hash = jQuery.parseJSON(res);

  var result = hash["result"]["section"];
  var pictureLinks = this.extractPicturesLinks(result.items);
  this.downloadPictures(pictureLinks);

  var perPage = result.pageSize;
  var totalCount = result.count;
  var startIndex = result.startIndex - 1;
  var totalPages = Math.ceil(totalCount / perPage);
  var currentPageNumber = totalPages - Math.floor((totalCount - startIndex)/ perPage);
  console.log("startIndex = " + startIndex);
  console.log("perPage = " + perPage);
  console.log("totalCount = " + totalCount);
  console.log("totalPages = " + totalPages);
  console.log("currentPageNumber = " + currentPageNumber);

  if(currentPageNumber != totalPages){
    nextStartIndex = startIndex + perPage + 1;
    console.log("Going to retrieve the next page starting at index " + nextStartIndex);
    this.downloadOnePage(nextStartIndex, this.site, this.nodeId);
  }
}

ShutterflyBackupService.prototype.extractPicturesLinks = function(arr){
  console.log("[extractPicturesLinks]");
  var names=[];
  for(var i=0; i<arr.length; i++){
    var link = this.constructDownloadLink(arr[i]);
    names.push(link);
  }
  return names;
}

/**
* Construct URL that is used to download the given picture item
*/
ShutterflyBackupService.prototype.constructDownloadLink= function(pictureItem){
  var shutterflyLink = "https://cmd.shutterfly.com/commands/async/downloadpicture?site=site&";
  shutterflyLink = shutterflyLink + "id=" + pictureItem.shutterflyId + "&";
  shutterflyLink = shutterflyLink + "collectionKey=" + this.collectionKey + "&";
  shutterflyLink = shutterflyLink + "albumKey=" + this.albumKey + "&";
  shutterflyLink = shutterflyLink + "title=" + pictureItem.title;
  return shutterflyLink;
}

/**
* Download a picture at URL to disk
*/
ShutterflyBackupService.prototype.downloadPictures = function(picturesLinks){
  console.log("[downloadPictures]");
  for(var i =0; i<picturesLinks.length; i++){
    if(this.debug == true){
      console.log("[downloadPictures]" + picturesLinks[i]);
    }else{
      this.saveToDisk(picturesLinks[i]);
    }
  }
  this.totalDownloaded += picturesLinks.length;
  console.log("[downloadPictures] Total cummulative downloaded: " + this.totalDownloaded);
}

//http://muaz-khan.blogspot.com/2012/10/save-files-on-disk-using-javascript-or.html
ShutterflyBackupService.prototype.saveToDisk = function(fileURL, fileName) {
  if (!window.ActiveXObject) {
    var save = document.createElement('a');
    save.href = fileURL;
    save.target = '_blank';
    save.download = fileName || 'unknown';

    var event = document.createEvent('Event');
    event.initEvent('click', true, true);
    save.dispatchEvent(event);
    (window.URL || window.webkitURL).revokeObjectURL(save.href);
  }
}

ShutterflyBackupService.prototype.parseAlbumProperties = function(){
  console.log("[parseAlbumProperties]");
  var albumUrl = this.albumUrl;

  console.log("[parseAlbumProperties]" + "albumUrl: " + albumUrl);

  if(match = albumUrl.match(/(http.?:\/\/)?(.+)\.shutterfly.com\/(\d+)/)){
    console.log(albumUrl + "is a valid LEGACY url");
    this.site = match[2];
    this.nodeId = match[3];
    this.legacyFormat = true;
  }
  else if(match = albumUrl.match(/(http.?:\/\/)?(.+)\.shutterfly.com\/pictures\/(\d+)/)){
    console.log(albumUrl + "is a valid NEW url");
    this.site = match[2];
    this.nodeId = match[3];
    this.legacyFormat = false;
  }
  else{
    throw ("[parseAlbumProperties]" + albumUrl + "is invalid");
  }

  if(typeof this.site === 'undefined'){
    throw "site is not found";
  }else if(typeof this.nodeId === 'undefined'){
    throw "nodeId is not found";
  }

  console.log("site = " + this.site);
  console.log("nodeId = " + this.nodeId);
}

ShutterflyBackupService.prototype.downloadOnePage = function(startIndex, site, nodeId){
  if(typeof site === "undefined"){
    console.log("Site is not provided");
    throw "Site is not provided";
  }
  url = "https://cmd.shutterfly.com/commands/pictures/getitems?site="+site+"&amp;"

  if(this.legacyFormat){
    page = site;
  }else{
    page = site + "/pictures";
  }

    // The url is https://[page].shutterfly.com/[nodeId]
    pictures = jQuery.post( url, {
      startIndex: startIndex,
             page: page,
             nodeId: nodeId,
             format: "json"
    }, jQuery.proxy(this.downloadCurrentPage, this) , "json"
    );

  pictures.done(function(data, status) {
    console.log( "second success" );
  })
  .fail(jQuery.proxy(this.downloadCurrentPage, this));
}

ShutterflyBackupService.prototype.downloadAlbum = function (){
  if(typeof this.collectionKey === "undefined"){
    throw "collectionKey has not been provided";
  }

  if(typeof this.albumKey === "undefined"){
    throw "albumKey has not been provided";
  }

  if(typeof this.site === "undefined"){
    console.log("site has not been provided");
    throw "site has not been provided";
  }

  var startIndex = 0;
  this.parseAlbumProperties();
  this.downloadOnePage(startIndex, this.site, this.nodeId);
}
