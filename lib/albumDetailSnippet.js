/*
 * We need to run some code to determine
 * the details of the current album
 */
function injectJs(link) {
  var scr = document.createElement('script');
  scr.type="text/javascript";
  scr.src=link;
  document.body.appendChild(scr);
}

injectJs(chrome.extension.getURL('lib/injected.js'));
