/*
 * Send a message to the content script with details about the album
 */
window.postMessage({ type: "FROM_PAGE", collectionKey: Shr.S.collectionKey, albumKey: Shr.S.albumKey, site: Shr.S.siteName }, "*");
