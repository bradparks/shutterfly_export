Download all pictures from your Shutterfly album to your computer

This simple extension allows you to download the contents of
your Shutterfly album to your computer for backup.

It can be added to Chrome via this [link](https://chrome.google.com/webstore/detail/dcfcbchldhelnhnohooelmmfdkkemnal)

# Manual
To manually run the service

## Add dependecies

```
newScript = document.createElement('script');
newScript.type = 'text/javascript';
newScript.src = 'https://code.jquery.com/jquery-2.1.1.min.js';
document.getElementsByTagName('head')[0].appendChild(newScript);

newScript = document.createElement('script');
newScript.type = 'text/javascript';
newScript.src = 'https://rawgit.com/freethenation/durable-json-lint/master/lib/durable-json-lint-with-dependencies.js';
document.getElementsByTagName('head')[0].appendChild(newScript);
```

## New format

`var albumUrl="https://nahiii.shutterfly.com/pictures/60";`

## Legacy format

`var albumUrl="https://theisles.shutterfly.com/27";`

## Download pictures

```
var service = new ShutterflyBackupService(Shr.S.collectionKey, Shr.S.albumKey, Shr.S.siteName);
service.albumUrl=albumUrl
service.debug = true
service.downloadAlbum();
```

## Deployment

```
brew install node

npm install jsonfile

npm install shelljs

```

When ready to run deployment:

`./deploy.js` will produce zip file to upload
