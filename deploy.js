#!/usr/local/bin/node

require('shelljs/global');

var jf = require('jsonfile');
var obj = jf.readFileSync("manifest.json");
var currentVersion = obj.version;

var archiveName = "shutterfly_" + currentVersion + ".zip"
var version = exec("zip -r " + archiveName + " *").output;
