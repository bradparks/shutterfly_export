var port = chrome.runtime.connect();

function handleMessage(request, sender, sendResponse) {
  if(request.message == "download"){
    var albumUrl = request.url;
    console.log("Received request to download images from " + albumUrl);
    window.shutterflyBackupService.albumUrl = albumUrl;
    window.shutterflyBackupService.downloadAlbum();
  }
}

chrome.runtime.onMessage.addListener(handleMessage);

/*
 * Before we can begin, the window needs to send us details about the album
 *
 * Example:
 *
 * window.postMessage({ type: "FROM_PAGE", collectionKey: Shr.S.collectionKey, albumKey: Shr.S.albumKey }, "*");
 */
window.addEventListener("message", function(event) {
  // We only accept messages from ourselves
  if (event.source != window){
    return;
  }

  if (event.data.type && (event.data.type == "FROM_PAGE")) {
    console.log("Collection key " + event.data.collectionKey);
    console.log("AlbumKey " + event.data.albumKey);
    console.log("Site " + event.data.site);
    window.shutterflyBackupService = new ShutterflyBackupService(event.data.collectionKey, event.data.albumKey, event.data.site);
  }
}, false);
